'use strict';

const Contact = require('../../models/contact.model');

describe("A contact suite", function() {
  it("empty fields", function() {
    const contact = new Contact();
    try {
      contact.validateSync()
    } catch (e) {
      expect(e.toString()).toBe("Expected ValidationError: Path `email` is required., Path `name` is required.");
    }
  });

  it("short name", function() {
    const contact = new Contact({email:"ddd", name:"2a"});
    expect(contact.validateSync().errors.name.path).toBe("name");
  });

  it("long name", function() {
    const contact = new Contact({email:"ddd", name:"user"});
    expect(contact.validateSync()).toBe(undefined);
  });

});
