'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    name: {type: String, required: true, validate: { validator: v => v && v.length > 2 } },
    email: {type: String, required: true}
}, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'}
})

ContactSchema.index({updated_at:-1})

module.exports = mongoose.model('Contact', ContactSchema);
