'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const koa = require('koa');
const app = koa();
const config = require('./config');

const mongoose = require('mongoose');

mongoose.connect(config.mongo.uri, config.mongo.options);

const Jade = require('koa-jade')
const jade = new Jade(config.jade);
jade.locals = {
  moment: require('moment')
};

jade.use(app);

const router = require('./routes');

app.use(router.routes());
/*
app.use(function *(){
  this.render('404');
});
*/
app.listen(config.port);
