'use strict';

const router = require('koa-router')();
const Contact = require('../models/contact.model');
const koaBody = require('koa-body')();

router.get('/', function *(){
  this.render('index');
})

router.get('/addcontact', function *() {
  const validation = {
    name:{},
    email:{}
  };
  const contact = {name:"", email:""};
  this.render('contact-new', {contact, validation});
})

router.post('/addcontact', koaBody, function *() {

  let contact = {
    name: this.request.body.name,
    email: this.request.body.email
  };

  contact = new Contact(contact);

  const validation = {
    name:{},
    email:{}
  };

  try {
    yield cb => contact.validate(cb);
  } catch(err) {
    for(let k in err.errors) {
        validation[k].error = err.errors[k];
    }
    // ошибка валидации
    this.render('contact-new', {contact, validation});
    return;
  }

  try {
    yield contact.save()
  } catch(err) {
    // ошибка сохранения
    this.render('contact-new', {contact, validation, saveError: true});
    return;
  }

  this.response.redirect('/listcontacts');
})

router.get('/showcontact/:id', function *() {
  const contact = yield Contact.findById(this.params.id);
  this.render('contact-show', {contact:contact});
})

router.get('/listcontacts', function *() {
  const count  = yield Contact.count();
  let query = Contact.find().sort({updated_at:-1});

  if (this.request.query.skip) {
    query = query.skip(parseInt(this.request.query.skip));
  }

  query =  query.limit(10);

  const contacts = yield query;
  const template = this.request.query.dataonly ? 'contact-list-data' : 'contact-list';
  this.render(template, {contacts: contacts, pagination:{count, step:10}});
})

module.exports = router;
