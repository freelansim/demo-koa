'use strict';

const path = require('path');

// Development specific configuration
// ==================================
module.exports = {
    // MongoDB connection options
    port:9000,
    mongo: {
        uri: 'mongodb://localhost/testkoa-dev'
    },
    jade: {
      debug: true,
      noCache: true
    }
};
